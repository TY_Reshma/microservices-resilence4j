package com.te.resilence4jnew;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Resilence4jnewApplication {

	public static void main(String[] args) {
		SpringApplication.run(Resilence4jnewApplication.class, args);
	}

}
