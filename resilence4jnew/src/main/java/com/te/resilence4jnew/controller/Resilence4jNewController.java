package com.te.resilence4jnew.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/second")
public class Resilence4jNewController {

	@GetMapping("/getResilence4jNew")
	public String serviceResilence4jNew() {
		return "serviceResilence4jNew called from First";
		
	}
}
