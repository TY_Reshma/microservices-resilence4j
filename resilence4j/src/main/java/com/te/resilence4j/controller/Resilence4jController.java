package com.te.resilence4j.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@RestController
@RequestMapping("/first")
public class Resilence4jController {

	@Autowired
	private RestTemplate restTemplate;
	
	private static final String url="http://localhost:9999/second/getResilence4jNew";
	private static final String SERVICE_FIRST="serviceFirst";
	
	@GetMapping("/getSecondService")
	@CircuitBreaker(name = SERVICE_FIRST,fallbackMethod = "fallBack")
	public String getResilence4jNew() {
		return restTemplate.getForObject(url, String.class);
	}
	
	public String fallBack(Exception e) {
		return "This is a fallback method for service first";
		
	}
	
	
	
	
	
}
